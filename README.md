# count-code
##count-code是什么
count-code是一个统计项目源代码的工具，基于JDK1.7和Maven管理的开源项目，暂时不依赖第三方jar，后期会根据扩展功能加入三方jar包（包括日志、统计图标等）。
##现有功能说明
###代码行数统计功能
>统计目标文件地址中所有文件的总行数、代码行数、空格行数、注释行数（暂时只支持Java注释格式）

###可增加扩展功能
>根据文件类型统计
>统计文件总大小
>排除统计的目录和文件
>根据文件类型和统计结果的维度绘制统计图表
>计算统计时间

##结构说明
count-code-core---代码统计核心包
  │
  ├src.main.java.com.weibo.framework.core┈┈┈┈┈实现类文件夹
  │  │
  │  ├CounterOneThread.java -------单线程统计（和多线程做对比）
  │  │
  │  ├CounterRecursiveTask.java----多线程协作统计
  │  │
  │  ├JavaCounter.java-------------具体业务逻辑处理
  │  │
  │  ├CounterException.java--------自定义异常类
  │  │
  │  ├CounterModel.java------------统计结果对象
  │  │
  │  ├FileUtils.java---------------文件操作工具类
  │  │
  │  ├Counter.java-----------------抽象统计接口（多文件可根据此接口添加扩展实现）
  │  │
  │  └CounterStartup.java----------统计功能入口
  │
  ├src.test.java.com.weibo.framework.core┈┈┈┈┈测试类文件夹
  │  │
  │  └CounterTest.java-------------单元测试类
  │  
  └README.md ----------------------说明文本
  
count-code-web---保留扩展包（暂时未写代码）

##源码地址
https://git.oschina.net/fattiger/count-code.git