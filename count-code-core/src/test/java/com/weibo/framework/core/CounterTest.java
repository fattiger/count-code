package com.weibo.framework.core;

import java.io.File;
import java.util.concurrent.ForkJoinPool;

import com.weibo.framework.core.concurrent.CounterRecursiveTask;
import com.weibo.framework.core.counter.Counter;
import com.weibo.framework.core.counter.JavaCounter;
import com.weibo.framework.core.model.CounterModel;

public class CounterTest {
	private final static ForkJoinPool forkJoinPool = new ForkJoinPool();  
//	private final static CounterOneThread codeCounterOneThread = new CounterOneThread();
	public static void main(String[] args){
		final long start = System.nanoTime();
		Counter counter = new JavaCounter();
		String filePath = "E:/fattiger git/dubbo-example";
		CounterModel counterModel = null;
		// one thread count
//		counterModel = codeCounterOneThread.getCodeConter(filePath, counter);
		// multi thread count
        counterModel = forkJoinPool.invoke(new CounterRecursiveTask(new File(filePath), counter));  
        final long end = System.nanoTime();  
	    System.out.println("TotalLineNo  : " + counterModel.getTotalLineNo());
	    System.out.println("CodeLineNo   : " + counterModel.getCodeLineNo());
	    System.out.println("BlankLineNo  : " + counterModel.getBlankLineNo());
	    System.out.println("CommentLineNo: " + counterModel.getCommentLineNo());
        System.out.println("Time taken   : " + (end - start) / 1.0e9);  
	}
}
