package com.weibo.framework.core.exception;

/**
 * define counter exception
 * @author fattiger.xiaoyang
 * @date 2016/03/22
 */
public class CounterException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	private static final String ERR_PRE = "E001";
	public static enum Type{
		NO_EXIST_DIRECTORY_ERROR(ERR_PRE + "001","目录或文件不存在!")
		;
		
		private String code;
		private String message;
		Type(String code,String message){
			this.code = code;
			this.message = message;
		}
		
		public String getMessage(){
			return this.message;
		}
		
		public String getCode(){
			return this.code;
		}
	}
	
	private Type type;
	
	public Type getType(){
		return type;
	}
	
	public CounterException(String message){
		super(message);
	}
	
	public CounterException(Type type){
		super(type.getCode() + ":" + type.getMessage());
	}
	
	public CounterException(Type type, String message){
		super(message + type.getCode() + ":" + type.getMessage());
	}
}
