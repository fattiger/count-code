package com.weibo.framework.core.concurrent;

import java.io.File;
import java.util.List;

import com.weibo.framework.core.counter.Counter;
import com.weibo.framework.core.model.CounterModel;
import com.weibo.framework.core.util.FileUtils;

/**
 * 单线程统计（测试用和多线程时间做对比）
 * @author fattiger.xiaoyang
 * @date 2016/03/23
 */
public class CounterOneThread {
	public CounterModel getCounter(String path, Counter counter){
		List<File> list = FileUtils.getAllFilesByPath(path);
		long totalLineNo = 0;
        long codeLineNo = 0;
        long blankLineNo = 0;
        long commentLineNo = 0;
	    for(File codeFile : list){
	    	CounterModel codeCounter = counter.getCounter(codeFile);
	    	totalLineNo += codeCounter.getTotalLineNo();
	        codeLineNo += codeCounter.getCodeLineNo();
	        blankLineNo += codeCounter.getBlankLineNo();
	        commentLineNo += codeCounter.getCommentLineNo();
	    }
	    CounterModel codeCounter = new CounterModel();
	    codeCounter.setTotalLineNo(totalLineNo);
		codeCounter.setCodeLineNo(codeLineNo);
		codeCounter.setBlankLineNo(blankLineNo);
		codeCounter.setCommentLineNo(commentLineNo);
		return codeCounter;
	}
}
