
package com.weibo.framework.core.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * FileUtils
 * @author fattiger.xiaoyang
 * @date 2016/03/22
 */
public class FileUtils {

	private final static List<String> EXCLUDE_DIRECTORY;
	private static List<File> fileList = new ArrayList<File>();
	static {
		EXCLUDE_DIRECTORY = new ArrayList<String>();
//		EXCLUDE_DIRECTORY.add(".settings");
//		EXCLUDE_DIRECTORY.add("target");
//		EXCLUDE_DIRECTORY.add(".git");
	}
	
	private final static List<String> EXCLUDE_FILE;
	static {
		EXCLUDE_FILE = new ArrayList<String>();
		EXCLUDE_FILE.add(".class");
		EXCLUDE_FILE.add(".project");
		EXCLUDE_FILE.add(".classpath");
		EXCLUDE_FILE.add(".jar");
		EXCLUDE_FILE.add(".prefs");
		EXCLUDE_FILE.add(".component");
		EXCLUDE_FILE.add(".gitignore");
		EXCLUDE_FILE.add(".md");
		EXCLUDE_FILE.add("LICENSE");
	}
	
	/**
	 * Get all file by path.
	 * @param path
	 * @return
	 */
    public static List<File> getAllFilesByPath(String path) {
        File rootFile = new File(path);
        File[] files = rootFile.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                if(!EXCLUDE_DIRECTORY.contains(file.getPath())){
                	getAllFilesByPath(file.getPath());
                }
            } else {
                fileList.add(file);
            }
        }
        return fileList;
    }

    /**
     * Get excluded file by path.
     * @param path
     * @return
     */
    public static List<File> getExcludedFilesByPath(String path) {
        List<File> allFileList = getAllFilesByPath(path);
        List<File> excludedFileList = new ArrayList<File>();
        for (File file : allFileList) {
            String fileExtName = file.getPath().toLowerCase();
            if(!EXCLUDE_FILE.contains(fileExtName)){
            	excludedFileList.add(file);
            }
        }
        return excludedFileList;
    }
    
}
