package com.weibo.framework.core.model;

/**
 * count model
 * @author fattiger.xiaoyang
 * @date 2016/03/22
 */
public class CounterModel {
	
	private String extName;       //扩展名（文件类型）
	private String totalSize;     //文件总大小
	private Long   totalLineNo;   //代码总行数
    private Long   codeLineNo;    //实际代码行数
    private Long   commentLineNo; //注释行数
    private Long   blankLineNo;   //空行数
    
	public String getExtName() {
		return extName;
	}
	public void setExtName(String extName) {
		this.extName = extName;
	}
	public String getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(String totalSize) {
		this.totalSize = totalSize;
	}
	public Long getTotalLineNo() {
		return totalLineNo;
	}
	public void setTotalLineNo(Long totalLineNo) {
		this.totalLineNo = totalLineNo;
	}
	public Long getCodeLineNo() {
		return codeLineNo;
	}
	public void setCodeLineNo(Long codeLineNo) {
		this.codeLineNo = codeLineNo;
	}
	public Long getCommentLineNo() {
		return commentLineNo;
	}
	public void setCommentLineNo(Long commentLineNo) {
		this.commentLineNo = commentLineNo;
	}
	public Long getBlankLineNo() {
		return blankLineNo;
	}
	public void setBlankLineNo(Long blankLineNo) {
		this.blankLineNo = blankLineNo;
	}
    
}
