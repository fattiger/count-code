package com.weibo.framework.core.counter;

import java.io.File;

import com.weibo.framework.core.model.CounterModel;

/**
 * count base interface
 * @author fattiger.xiaoyang
 * @date 2016/03/22
 */
public interface Counter {
	CounterModel getCounter(File file);
}
