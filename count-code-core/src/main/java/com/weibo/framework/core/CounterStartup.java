package com.weibo.framework.core;

import java.io.File;
import java.util.concurrent.ForkJoinPool;

import com.weibo.framework.core.concurrent.CounterRecursiveTask;
import com.weibo.framework.core.counter.Counter;
import com.weibo.framework.core.counter.JavaCounter;
import com.weibo.framework.core.exception.CounterException;
import com.weibo.framework.core.model.CounterModel;

/**
 * 统计服务启动类
 * @author fattiger.xiaoyang
 * @date 2016/03/23
 */
public class CounterStartup {
	private final static ForkJoinPool forkJoinPool = new ForkJoinPool();  
	public static void main(String[] args){
		String filePath = "D:/fattiger git/dubbo-extension";
		if (args != null && args.length != 0) {
        	filePath = args[0];
        }
		File file = new File(filePath);
		//判断所输文件路径是否正确
		if(!file.exists() && !file.isDirectory()){
			System.out.println(CounterException.Type.NO_EXIST_DIRECTORY_ERROR.getMessage());
			throw new CounterException(CounterException.Type.NO_EXIST_DIRECTORY_ERROR);
		}
		//统计时间开始
		final long start = System.nanoTime();
		//默认按照Java实现接口统计文件 扩展可根据文件扩展名 反射切换不同的实现并且缓存实现类
		Counter counter = new JavaCounter();
		//并发统计文件返回统计结果
		CounterModel counterModel = forkJoinPool.invoke(new CounterRecursiveTask(new File(filePath), counter));  
        //统计时间结束
		final long end = System.nanoTime();
		//输出统计结果及执行时间
	    System.out.println("TotalLineNo  : " + counterModel.getTotalLineNo());
	    System.out.println("CodeLineNo   : " + counterModel.getCodeLineNo());
	    System.out.println("BlankLineNo  : " + counterModel.getBlankLineNo());
	    System.out.println("CommentLineNo: " + counterModel.getCommentLineNo());
        System.out.println("Time taken   : " + (end - start) / 1.0e9);  
	}
}
